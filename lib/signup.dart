import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_vehicles/controllers/app_controller.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';

class ApplyPage extends StatefulWidget {
  @override
  _ApplyPageState createState() => _ApplyPageState();
}

class _ApplyPageState extends State<ApplyPage> {
  static const double _gap = 12.0;
  static const double _hintSize = 15.0;
  static const Color _hintColor = Color(0xFFB7C2CE);
  static const Color _inputBorderColor = Color(0xFFD3DAE2);
  ApplyPageController _controller = Get.put(ApplyPageController());
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _idController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _phoneNoController = TextEditingController();

  @override
  void initState() {
    _controller.setControllers(
      _nameController,
      _idController,
      _phoneNoController,
      _passwordController,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
            size: Numbers.appBarIconSize,
          ),
          onPressed: () => Get.back(),
        ),
        title: Text(
          Strings.signupTitle,
          style: TextStyle(
            fontSize: Numbers.defaultAppBarTitleSize,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        shadowColor: Colors.transparent,
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: Numbers.basicGap),
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  children: [
                    SizedBox(height: _gap),

                    /// Unit
                    Text(
                      Strings.captionUnit,
                      style: TextStyle(
                          fontSize: Numbers.defaultCaptionSize,
                          color: AppColors.textGray),
                    ),
                    SizedBox(height: _gap),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: Numbers.basicGap),
                      height: Numbers.defaultControlHeight,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius:
                            BorderRadius.circular(Numbers.defaultRadius),
                        border: Border.all(color: _inputBorderColor),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: Obx(
                          () => DropdownButton<String>(
                            icon: Icon(
                              Icons.arrow_drop_down,
                              size: 36,
                              color: AppColors.iconDarkGray,
                            ),
                            isExpanded: true,
                            value: _controller.unit,
                            items: List<DropdownMenuItem<String>>.generate(
                              kMilitaryUnit.length,
                              (index) => DropdownMenuItem<String>(
                                value: kMilitaryUnit[index],
                                child: Text(
                                  kMilitaryUnit[index],
                                  style: TextStyle(
                                    color: AppColors.textBlack,
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                              ),
                            ),
                            onChanged: (value) {
                              _controller.unit = value!;
                            },
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: _gap),

                    /// Rank
                    Text(
                      Strings.captionRank,
                      style: TextStyle(
                          fontSize: Numbers.defaultCaptionSize,
                          color: AppColors.textGray),
                    ),
                    SizedBox(height: _gap),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: Numbers.basicGap),
                      height: Numbers.defaultControlHeight,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius:
                            BorderRadius.circular(Numbers.defaultRadius),
                        border: Border.all(color: _inputBorderColor),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: Obx(
                          () => DropdownButton<String>(
                            icon: Icon(
                              Icons.arrow_drop_down,
                              size: 36,
                              color: AppColors.iconDarkGray,
                            ),
                            isExpanded: true,
                            value: _controller.rank,
                            items: List<DropdownMenuItem<String>>.generate(
                              kMilitaryRank.length,
                              (index) => DropdownMenuItem<String>(
                                value: kMilitaryRank[index],
                                child: Text(
                                  kMilitaryRank[index],
                                  style: TextStyle(
                                    color: AppColors.textBlack,
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                              ),
                            ),
                            onChanged: (value) {
                              _controller.rank = value!;
                            },
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: _gap),

                    /// Name
                    Text(
                      Strings.captionName,
                      style: TextStyle(
                          fontSize: Numbers.defaultCaptionSize,
                          color: AppColors.textGray),
                    ),
                    SizedBox(height: _gap),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: Numbers.basicGap),
                      height: Numbers.defaultControlHeight,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius:
                            BorderRadius.circular(Numbers.defaultRadius),
                        border: Border.all(color: _inputBorderColor),
                      ),
                      child: TextField(
                        controller: _nameController,
                        autocorrect: false,
                        obscureText: false,
                        onChanged: (newValue) {
                          _controller.checkCapable();
                        },
                        decoration: InputDecoration(
                          hintText: Strings.hintName,
                          hintStyle:
                              TextStyle(color: _hintColor, fontSize: _hintSize),
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                        ),
                      ),
                    ),
                    SizedBox(height: _gap),

                    /// Phone
                    Text(
                      Strings.captionPhone,
                      style: TextStyle(
                          fontSize: Numbers.defaultCaptionSize,
                          color: AppColors.textGray),
                    ),
                    SizedBox(height: _gap),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: Numbers.basicGap),
                      height: Numbers.defaultControlHeight,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius:
                            BorderRadius.circular(Numbers.defaultRadius),
                        border: Border.all(color: _inputBorderColor),
                      ),
                      child: TextField(
                        controller: _phoneNoController,
                        autocorrect: false,
                        obscureText: false,
                        onChanged: (newValue) {
                          _controller.checkCapable();
                        },
                        decoration: InputDecoration(
                          hintText: Strings.hintPhone,
                          hintStyle:
                              TextStyle(color: _hintColor, fontSize: _hintSize),
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                        ),
                      ),
                    ),
                    SizedBox(height: _gap),

                    /// ID
                    Text(
                      Strings.captionId,
                      style: TextStyle(
                          fontSize: Numbers.defaultCaptionSize,
                          color: AppColors.textGray),
                    ),
                    SizedBox(height: _gap),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: Numbers.basicGap),
                      height: Numbers.defaultControlHeight,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius:
                            BorderRadius.circular(Numbers.defaultRadius),
                        border: Border.all(color: _inputBorderColor),
                      ),
                      child: TextField(
                        controller: _idController,
                        autocorrect: false,
                        obscureText: false,
                        onChanged: (newValue) {
                          _controller.checkCapable();
                        },
                        decoration: InputDecoration(
                          hintText: Strings.hintIdInput,
                          hintStyle:
                              TextStyle(color: _hintColor, fontSize: _hintSize),
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                        ),
                      ),
                    ),
                    SizedBox(height: _gap),

                    /// Password
                    Text(
                      Strings.captionPass,
                      style: TextStyle(
                          fontSize: Numbers.defaultCaptionSize,
                          color: AppColors.textGray),
                    ),
                    SizedBox(height: _gap),
                    Container(
                      padding: EdgeInsets.only(
                        left: Numbers.basicGap,
                      ),
                      height: Numbers.defaultControlHeight,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius:
                            BorderRadius.circular(Numbers.defaultRadius),
                        border: Border.all(color: _inputBorderColor),
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Obx(
                              () => TextField(
                                controller: _passwordController,
                                autocorrect: false,
                                obscureText: _controller.isObscurePassword,
                                onChanged: (newValue) {
                                  _controller.checkCapable();
                                },
                                decoration: InputDecoration(
                                  hintText: Strings.hintPass,
                                  hintStyle: TextStyle(
                                      color: _hintColor, fontSize: _hintSize),
                                  border: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              _controller.isObscurePassword =
                                  !_controller.isObscurePassword;
                            },
                            icon: Icon(
                              Icons.remove_red_eye,
                              size: Numbers.obscureIconSize,
                              color: AppColors.iconGray,
                            ),
                            iconSize: Numbers.obscureIconSize,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: _gap),

                    /// Driver Class
                    Text(
                      Strings.captionDriverClass,
                      style: TextStyle(
                          fontSize: Numbers.defaultCaptionSize,
                          color: AppColors.textGray),
                    ),
                    SizedBox(height: _gap),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: Numbers.basicGap),
                      height: Numbers.defaultControlHeight,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius:
                            BorderRadius.circular(Numbers.defaultRadius),
                        border: Border.all(color: _inputBorderColor),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: Obx(
                          () => DropdownButton<DriverClass>(
                            icon: Icon(
                              Icons.arrow_drop_down,
                              size: 36,
                              color: AppColors.iconDarkGray,
                            ),
                            isExpanded: true,
                            value: _controller.driverClass,
                            items: DriverClass.values
                                .map(
                                  (e) => DropdownMenuItem<DriverClass>(
                                    value: e,
                                    child: Text(
                                      Strings.driverClassValues[e.index],
                                      style: TextStyle(
                                        color: AppColors.textBlack,
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                )
                                .toList(),
                            onChanged: (value) {
                              _controller.driverClass = value!;
                            },
                          ),
                        ),
                      ),
                    ),

                    SizedBox(height: 16),
                    Obx(
                      () {
                        return _controller.isError
                            ? Container(
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.error_outline,
                                      size: 16,
                                      color: AppColors.iconAlertRed,
                                    ),
                                    SizedBox(width: 4),
                                    Text(
                                      Strings.signupError,
                                      style: TextStyle(
                                        fontSize: 13.0,
                                        fontWeight: FontWeight.normal,
                                        color: AppColors.textAlertRed,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container();
                      },
                    ),
                    SizedBox(height: 16),

                    InkWell(
                      onTap: () {
                        _submit();
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: Numbers.basicGap),
                        height: Numbers.defaultControlHeight,
                        decoration: BoxDecoration(
                          color: AppColors.mainBlue,
                          borderRadius:
                              BorderRadius.circular(Numbers.defaultRadius),
                        ),
                        child: Obx(
                          () => Center(
                            child: _controller.isWaiting
                                ? SizedBox(
                                    width: Numbers.defaultIndicatorSize,
                                    height: Numbers.defaultIndicatorSize,
                                    child: CircularProgressIndicator(
                                      color: Colors.white,
                                    ),
                                  )
                                : Text(
                                    Strings.signupApply,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future _submit() async {
    dynamic ret = await _controller.submit();
    if (ret != null) {
      if (ret is bool && ret) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('신청이 완료되었습니다.'),
            duration: Duration(seconds: 1),
          ),
        );
        Get.back();
      } else if (ret is String) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(ret),
            duration: Duration(seconds: 1),
          ),
        );
      }
    }
  }
}

class ApplyPageController extends GetxController {
  RxBool _isObscure = true.obs;
  bool get isObscurePassword => _isObscure.value;
  set isObscurePassword(value) => _isObscure.value = value;

  RxBool _isCapable = false.obs;
  bool get isCapable => _isCapable.value;

  Rx<DriverClass> _driverClass = DriverClass.A.obs;
  DriverClass get driverClass => _driverClass.value;
  set driverClass(value) {
    _driverClass.value = value;
    update();
  }

  RxString _unit = kMilitaryUnit[0].obs;
  String get unit => _unit.value;
  set unit(value) => _unit.value = value;

  RxString _rank = kMilitaryRank[0].obs;
  String get rank => _rank.value;
  set rank(value) => _rank.value = value;

  late TextEditingController _nameController;
  late TextEditingController _idController;
  late TextEditingController _phoneNoController;
  late TextEditingController _passwordController;

  setControllers(
    TextEditingController name,
    TextEditingController id,
    TextEditingController phone,
    TextEditingController pass,
  ) {
    _nameController = name;
    _idController = id;
    _phoneNoController = phone;
    _passwordController = pass;
  }

  void checkCapable() {
    _isCapable.value = _nameController.text.isNotEmpty &&
        _idController.text.isNotEmpty &&
        _phoneNoController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty;
  }

  RxBool _isWaiting = false.obs;
  bool get isWaiting => _isWaiting.value;

  RxBool _isError = false.obs;
  bool get isError => _isError.value;
  set isError(value) => _isError.value = value;

  Future submit() async {
    checkCapable();
    if (_isCapable.value) {
      AppController appController = Get.find();
      _isWaiting.value = true;
      var response = await appController.signup(
        '${_driverClass.value.index}',
        _idController.text,
        _rank.value,
        _passwordController.text,
        _phoneNoController.text,
        _unit.value,
        _nameController.text,
      );
      if (response.statusCode == 200) {
        Map<String, dynamic> body = response.body;
        if (body['code'] != null && body['code'] == 200) {
          return true;
        }
      }
    }
    _isError.value = true;
    return null;
  }
}
