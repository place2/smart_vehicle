import 'package:intl/intl.dart';

extension DateTimeFunction on DateTime {
  bool sameDay(DateTime other) {
    return this.year == other.year &&
        this.month == other.month &&
        this.day == other.day;
  }

  bool isToday() {
    return this.sameDay(DateTime.now());
  }

  String dateString({bool containsYear = false}) {
    if (containsYear) {
      return DateFormat('y년 MM월 dd일 (E)', 'ko_KR').format(this);
    }
    return DateFormat('MM.dd (E)', 'ko_KR').format(this);
  }

  String timeString() {
    return DateFormat('a hh:mm', 'ko_KR').format(this);
  }
}
