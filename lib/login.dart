import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';
import 'package:smart_vehicles/controllers/login_controller.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  static const double titleFontSize = 28.0;
  static const double defaultFontSize = 15.0;

  final LoginPageController _controller = Get.put(LoginPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/login_background.png'),
              fit: BoxFit.fill),
        ),
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(Numbers.basicGap),
            width: double.infinity,
            height: double.infinity,
            color: Colors.transparent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Center(
                    child: RichText(
                      text: TextSpan(
                          style: TextStyle(
                            fontSize: titleFontSize,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                          children: [
                            TextSpan(
                              text: Strings.titleSmart,
                              style: TextStyle(
                                color: AppColors.textGreen,
                              ),
                            ),
                            TextSpan(
                              text: '\n${Strings.titleRemains}',
                            ),
                          ]),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: Numbers.basicGap),
                  height: Numbers.defaultControlHeight,
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(Numbers.defaultRadius),
                    border: Border.all(color: AppColors.inputBorderColor),
                  ),
                  child: TextField(
                    textInputAction: TextInputAction.next,
                    controller: _controller.idController,
                    autocorrect: false,
                    obscureText: false,
                    onChanged: (newValue) {
                      _controller.checkCapable();
                    },
                    decoration: InputDecoration(
                      hintText: Strings.hintId,
                      hintStyle: TextStyle(
                          color: Colors.white, fontSize: defaultFontSize),
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                    ),
                    style: TextStyle(
                        color: Colors.white, fontSize: defaultFontSize),
                  ),
                ),
                SizedBox(height: 12),
                Container(
                  padding: EdgeInsets.only(left: Numbers.basicGap, right: 6),
                  height: Numbers.defaultControlHeight,
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(Numbers.defaultRadius),
                    border: Border.all(color: AppColors.inputBorderColor),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Obx(
                          () => TextField(
                            textInputAction: TextInputAction.go,
                            controller: _controller.passwordController,
                            autocorrect: false,
                            obscureText: _controller.isObscurePassword,
                            onChanged: (newValue) {
                              _controller.checkCapable();
                            },
                            onSubmitted: (value) {
                              _controller.login(context);
                            },
                            decoration: InputDecoration(
                              hintText: Strings.hintPassword,
                              hintStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: defaultFontSize),
                              border: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              focusedBorder: InputBorder.none,
                            ),
                            style: TextStyle(
                                color: Colors.white, fontSize: defaultFontSize),
                          ),
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          _controller.isObscurePassword =
                              !_controller.isObscurePassword;
                        },
                        icon: Icon(
                          Icons.remove_red_eye,
                          size: Numbers.obscureIconSize,
                          color: AppColors.iconTranslucentWhite,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 12),
                _controller.errorWidget(),
                SizedBox(height: 12),
                InkWell(
                  onTap: () {
                    _controller.login(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColors.mainBlue,
                      borderRadius:
                          BorderRadius.circular(Numbers.defaultRadius),
                    ),
                    width: double.infinity,
                    height: Numbers.defaultControlHeight,
                    child: _controller.buttonLabel(),
                    // Obx(
                    //   () => Center(
                    //     child: _controller.isWaiting
                    //         ? SizedBox(
                    //             width: Numbers.defaultIndicatorSize,
                    //             height: Numbers.defaultIndicatorSize,
                    //             child: CircularProgressIndicator(
                    //               color: Colors.white,
                    //             ),
                    //           )
                    //         : Text(
                    //             Strings.login,
                    //             style: TextStyle(
                    //               fontSize: defaultFontSize,
                    //               color: Colors.white,
                    //               fontWeight: FontWeight.normal,
                    //             ),
                    //           ),
                    //   ),
                    // ),
                  ),
                ),
                SizedBox(height: 50),
                InkWell(
                  onTap: () {
                    _controller.apply();
                  },
                  child: Container(
                    width: double.infinity,
                    height: 20,
                    child: Center(
                      child: RichText(
                        text: TextSpan(
                          style: TextStyle(
                            fontSize: 13,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                          children: [
                            TextSpan(
                              text: Strings.noAccount,
                            ),
                            TextSpan(
                              text: Strings.pleaseSingup,
                              style: TextStyle(
                                color: AppColors.mainBlue,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ],
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 50),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
