import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_vehicles/controllers/app_controller.dart';
import 'package:smart_vehicles/data/operation_model.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';
import 'package:smart_vehicles/controllers/safety_confirm_controller.dart';

class SafetyConfirm extends StatelessWidget {
  final OperationModel plan;
  SafetyConfirm(this.plan);

  final SafetyConfirmController _controller =
      Get.put(SafetyConfirmController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: AppColors.iconBlack,
            size: Numbers.appBarIconSize,
          ),
          onPressed: () => Get.back(),
        ),
        title: Text(
          Strings.safetyConfirmTitle,
          style: TextStyle(
            color: AppColors.textBlack,
            fontSize: Numbers.defaultAppBarTitleSize,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        shadowColor: Colors.transparent,
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(
            left: Numbers.basicGap,
            right: Numbers.basicGap,
            bottom: 37.0,
          ),
          color: Colors.white,
          width: double.infinity,
          height: double.infinity,
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  children: [
                    SizedBox(height: 50),
                    _controller.safetyItem(0, Strings.safetyCaption1),
                    // Container(
                    //   height: Numbers.safetyCheckIconSize,
                    //   child: Row(
                    //     children: [
                    //       InkWell(
                    //         onTap: () => _controller.check(
                    //           0,
                    //           !_controller.isChecked(0),
                    //         ),
                    //         child: Obx(
                    //           () => Icon(
                    //             Icons.check_circle,
                    //             color: _controller.isChecked(0)
                    //                 ? AppColors.mainBlue
                    //                 : AppColors.iconDisabledGray,
                    //             size: Numbers.safetyCheckIconSize,
                    //           ),
                    //         ),
                    //       ),
                    //       SizedBox(width: 14),
                    //       Text(
                    //         Strings.safetyCaption1,
                    //         style: TextStyle(
                    //           color: AppColors.textBlack,
                    //           fontSize: Numbers.defaultButtonCaptionFontSize,
                    //           fontWeight: FontWeight.normal,
                    //           height: 1.2,
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    SizedBox(height: 33),
                    _controller.safetyItem(1, Strings.safetyCaption2),
                    // Container(
                    //   height: Numbers.safetyCheckIconSize,
                    //   child: Row(
                    //     children: [
                    //       InkWell(
                    //         onTap: () => _controller.check(
                    //           1,
                    //           !_controller.isChecked(1),
                    //         ),
                    //         child: Obx(
                    //           () => Icon(
                    //             Icons.check_circle,
                    //             color: _controller.isChecked(1)
                    //                 ? AppColors.mainBlue
                    //                 : AppColors.iconDisabledGray,
                    //             size: Numbers.safetyCheckIconSize,
                    //           ),
                    //         ),
                    //       ),
                    //       SizedBox(width: 14),
                    //       Text(
                    //         Strings.safetyCaption2,
                    //         style: TextStyle(
                    //           color: AppColors.textBlack,
                    //           fontSize: Numbers.defaultButtonCaptionFontSize,
                    //           fontWeight: FontWeight.normal,
                    //           height: 1.2,
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    SizedBox(height: 33),
                    _controller.safetyItem(2, Strings.safetyCaption3),
                    // Container(
                    //   height: Numbers.safetyCheckIconSize,
                    //   child: Row(
                    //     children: [
                    //       InkWell(
                    //         onTap: () => _controller.check(
                    //           2,
                    //           !_controller.isChecked(2),
                    //         ),
                    //         child: Obx(
                    //           () => Icon(
                    //             Icons.check_circle,
                    //             color: _controller.isChecked(2)
                    //                 ? AppColors.mainBlue
                    //                 : AppColors.iconDisabledGray,
                    //             size: Numbers.safetyCheckIconSize,
                    //           ),
                    //         ),
                    //       ),
                    //       SizedBox(width: 14),
                    //       Text(
                    //         Strings.safetyCaption3,
                    //         style: TextStyle(
                    //           color: AppColors.textBlack,
                    //           fontSize: Numbers.defaultButtonCaptionFontSize,
                    //           fontWeight: FontWeight.normal,
                    //           height: 1.2,
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    SizedBox(height: 33),
                    _controller.safetyItem(3, Strings.safetyCaption4),
                    // Container(
                    //   height: Numbers.safetyCheckIconSize,
                    //   child: Row(
                    //     children: [
                    //       InkWell(
                    //         onTap: () => _controller.check(
                    //           3,
                    //           !_controller.isChecked(3),
                    //         ),
                    //         child: Obx(
                    //           () => Icon(
                    //             Icons.check_circle,
                    //             color: _controller.isChecked(3)
                    //                 ? AppColors.mainBlue
                    //                 : AppColors.iconDisabledGray,
                    //             size: Numbers.safetyCheckIconSize,
                    //           ),
                    //         ),
                    //       ),
                    //       SizedBox(width: 14),
                    //       Text(
                    //         Strings.safetyCaption4,
                    //         style: TextStyle(
                    //           color: AppColors.textBlack,
                    //           fontSize: Numbers.defaultButtonCaptionFontSize,
                    //           fontWeight: FontWeight.normal,
                    //           height: 1.2,
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    SizedBox(height: 33),
                    _controller.safetyItem(4, Strings.safetyCaption5),
                    // Container(
                    //   height: Numbers.safetyCheckIconSize,
                    //   child: Row(
                    //     children: [
                    //       InkWell(
                    //         onTap: () => _controller.check(
                    //           4,
                    //           !_controller.isChecked(4),
                    //         ),
                    //         child: Obx(
                    //           () => Icon(
                    //             Icons.check_circle,
                    //             color: _controller.isChecked(4)
                    //                 ? AppColors.mainBlue
                    //                 : AppColors.iconDisabledGray,
                    //             size: Numbers.safetyCheckIconSize,
                    //           ),
                    //         ),
                    //       ),
                    //       SizedBox(width: 14),
                    //       Text(
                    //         Strings.safetyCaption5,
                    //         style: TextStyle(
                    //           color: AppColors.textBlack,
                    //           fontSize: Numbers.defaultButtonCaptionFontSize,
                    //           fontWeight: FontWeight.normal,
                    //           height: 1.2,
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                  ],
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: Obx(
                  () => TextButton(
                    onPressed: () {
                      if (!plan.confirmStatus) {
                        _controller.beginDrive(context, plan);
                      }
                    },
                    child: _controller.isWaiting
                        ? SizedBox(
                            width: 18,
                            height: 18,
                            child: CircularProgressIndicator(
                              color: Colors.white,
                            ),
                          )
                        : Text(
                            Strings.safetyButtonCaption,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: Numbers.defaultButtonCaptionFontSize,
                              fontWeight: FontWeight.normal,
                              height: 1.2,
                            ),
                          ),
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(
                        EdgeInsets.symmetric(vertical: 15),
                      ),
                      backgroundColor: (_controller.isCapable &&
                              !plan.confirmStatus)
                          ? MaterialStateProperty.all<Color>(AppColors.mainBlue)
                          : MaterialStateProperty.all<Color>(
                              AppColors.translucentMainBlue),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(Numbers.defaultRadius),
                          side: BorderSide.none,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
