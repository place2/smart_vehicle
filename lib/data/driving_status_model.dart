import 'package:geolocator/geolocator.dart';
import 'package:smart_vehicles/controllers/app_controller.dart';
import 'package:smart_vehicles/data/operation_model.dart';

class DrivingNotificationModel {
  int operationId;
  String createDate;
  int notiId;
  int type;

  DrivingNotificationModel(
      this.operationId, this.createDate, this.notiId, this.type);

  DrivingNotificationModel.fromJson(json)
      : operationId = json['carsInOperationID'] ?? 0,
        createDate = json['createDate'] ?? '',
        notiId = json['notifiID'] ?? 0,
        type = json['notifyType'] ?? 0;
}

class TP {
  int tpId;
  int routeId;
  String name;
  double lat;
  double lng;

  TP(this.tpId, this.routeId, this.name, this.lat, this.lng);

  TP.fromJson(json)
      : tpId = json['tpID'] ?? 0,
        routeId = json['routeID'] ?? 0,
        name = json['tpNickName'] ?? '',
        lat = json['latitude'] ?? 0,
        lng = json['longitude'] ?? 0;
}

class DrivingStatusModel {
  int operationId;
  double nextTpDistance;
  String nextTpName;
  DrivingNotificationModel? notification;
  double startLat;
  double startLng;
  double destLat;
  double destLng;
  int timeSecond;
  List<TP>? tpList;

  DrivingStatusModel(
    this.operationId,
    this.nextTpDistance,
    this.nextTpName,
    this.notification,
    this.startLat,
    this.startLng,
    this.destLat,
    this.destLng,
    this.timeSecond,
    this.tpList,
  );

  DrivingStatusModel.fromJson(json)
      : operationId = json['carsInOperationID'] ?? 0,
        nextTpDistance = json['nextTpDistance'] ?? 0,
        nextTpName = json['nextTpName'] ?? '',
        notification = json['notification'] != null
            ? DrivingNotificationModel.fromJson(json['notification'])
            : null,
        startLat = json['requiredDepartGPSLatitude'],
        startLng = json['requiredDepartGPSLongitude'],
        destLat = json['requiredDestGPSLatitude'],
        destLng = json['requiredDestGPSLongitude'],
        timeSecond = json['timeSecond'] ?? 0,
        tpList = json['tpList'] == null
            ? null
            : List<TP>.generate(
                json['tpList'].length,
                (index) {
                  return TP.fromJson(json['tpList'][index]);
                },
              );
}
