import 'package:geolocator/geolocator.dart';
import 'package:smart_vehicles/controllers/app_controller.dart';
import 'package:smart_vehicles/data/driving_status_model.dart';
import 'package:smart_vehicles/data/operation_model.dart';

class Model {
  /////////////////////////////////////////////////////////////////////////////
  /// Operations - Get Dashboard List
  Future operations() async {
    var response = await AppController.api.getDashboardList();
    if (response != null &&
        response.statusCode == 200 &&
        response.body['data'] != null) {
      List<OperationModel> plans = <OperationModel>[];
      List<dynamic> data = response.body['data']['dataList'];
      for (var item in data) {
        OperationModel plan = OperationModel.fromJson(item);
        plans.add(plan);
      }
      return plans;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  /// Operation Detail - Get Dashboard Detail
  Future details(int id) async {
    var response = await AppController.api.getDashboardDetail(id);
    if (response != null &&
        response.statusCode == 200 &&
        response.body['data'] != null) {
      OperationModel plan = OperationModel.fromJson(response.body['data']);
      return plan;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  /// Operation History - Get Dashboard History
  int historyPage = 0;
  int maxPage = 0;
  Future history() async {
    var response = await AppController.api.getDashboardHistory();
    if (response != null &&
        response.statusCode == 200 &&
        response.body['data'] != null) {
      List<OperationModel> plans = <OperationModel>[];
      List<dynamic> data = response.body['data']['dataList'];
      historyPage = response.body['data']['currentPage'];
      maxPage = response.body['data']['pagingEndNum'];
      for (var item in data) {
        OperationModel plan = OperationModel.fromJson(item);
        plans.add(plan);
      }
      return plans;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  /// More Operation History - Get Dashboard History
  Future moreHistory() async {
    var response = await AppController.api.getDashboardHistory(
      page: historyPage + 1,
    );
    if (response != null &&
        response.statusCode == 200 &&
        response.body['data'] != null) {
      List<OperationModel> plans = <OperationModel>[];
      List<dynamic> data = response.body['data']['dataList'];
      historyPage = response.body['data']['currentPage'];
      maxPage = response.body['data']['pagingEndNum'];
      for (var item in data) {
        OperationModel plan = OperationModel.fromJson(item);
        plans.add(plan);
      }
      return plans;
    }
    return null;
  }

  /////////////////////////////////////////////////////////////////////////////
  /// Driving Status - Get Map Detail API
  Future drivingStatus() async {
    var appController = AppController.shared;
    await appController.refreshPosition();

    OperationModel? plan = appController.currentPlan;
    Position? position = appController.currentPosition;
    if (plan == null || position == null) return null;

    var response = await AppController.api.getMapDetail(
      plan.id,
      position.latitude,
      position.longitude,
    );

    if (response != null &&
        response.statusCode == 200 &&
        response.body['data'] != null) {
      var data = response.body['data'];
      DrivingStatusModel status = DrivingStatusModel.fromJson(data);
      return status;
    }
    return null;
  }

  /////////////////////////////////////////////////////////////////////////////
  /// Singleton
  static final Model _instance = Model._internal();
  factory Model() {
    return _instance;
  }
  Model._internal() {
    //
  }
}
