import 'package:intl/intl.dart';

class OperationModel {
  int id;
  DateTime startTime = DateTime.now();
  String carType;
  String carNo;
  String driver;
  String leader;
  DateTime arrivalTime = DateTime.now();
  DateTime returnTime = DateTime.now();
  int distance;
  int cargoWeight = 0;
  List<String> passengers;
  int controlNo;
  String distanceType;
  String peopleOrCargo;
  String planUnit;
  String startPointName;
  String destinationName;
  bool confirmStatus = false;

  OperationModel(
    this.id,
    this.startTime,
    this.carType,
    this.carNo,
    this.driver,
    this.leader,
    this.arrivalTime,
    this.returnTime,
    this.distance,
    this.cargoWeight,
    this.passengers,
    this.controlNo,
    this.distanceType,
    this.peopleOrCargo,
    this.planUnit,
    this.startPointName,
    this.destinationName,
  );

  OperationModel.fromJson(var json)
      : carNo = json['carLicenseNo'] ?? '',
        planUnit = json['carOwner'] ?? '',
        passengers = [],
        leader = json['carResponsiblePerson'] ?? '',
        carType = json['carType'] ?? '',
        id = json['carsInOperationID'] ?? 0,
        confirmStatus = json['confirmStatus'].toLowerCase() == 'y',
        controlNo = json['departureOrderNum'] ?? 0,
        distanceType = json['distanceLevelStr'] ?? '',
        peopleOrCargo = json['personOrCargoStr'] ?? '',
        startPointName = json['requiredDepartureName'] ?? '',
        destinationName = json['requiredDestinationName'] ?? '',
        distance = json['totalDistance'] ?? 0,
        driver = json['userFullName'] ?? '' {
    var carPassengers = json['carPassengers'];
    if (carPassengers is String) {
      passengers.addAll(carPassengers.split(','));
    } else if (carPassengers is List<String>) {
      passengers.addAll(carPassengers);
    }
    arrivalTime =
        DateFormat('yyyy-MM-dd hh:mm').parse(json['requiredArrivalDateTime']);
    startTime =
        DateFormat('yyyy-MM-dd hh:mm').parse(json['requiredDepartureDateTime']);
    returnTime =
        DateFormat('yyyy-MM-dd hh:mm').parse(json['requiredReturnedDateTime']);
  }

  OperationModel.generateSample()
      : id = 0,
        startTime = DateTime.now(),
        carType = '차종 1',
        carNo = '00육1234',
        driver = '운전자 1',
        leader = '책임자',
        arrivalTime = DateTime.now().add(Duration(hours: 2)),
        returnTime = DateTime.now().add(Duration(hours: 4)),
        distance = 100,
        cargoWeight = 100,
        passengers = [
          '책임자',
          '탑승자 1',
          '탑승자 2',
          '탑승자 3',
          '탑승자 4',
          '탑승자 5',
          '운전자 1'
        ],
        controlNo = 10,
        distanceType = '장거리',
        peopleOrCargo = '값',
        startPointName = '출발',
        destinationName = '도착',
        planUnit = '단';
}
