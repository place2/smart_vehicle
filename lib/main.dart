import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:smart_vehicles/data/operation_model.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/controllers/app_controller.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decode/jwt_decode.dart';

import 'login.dart';
import 'home.dart';
import 'driving.dart';

Future<bool> checkToken() async {
  try {
    FlutterSecureStorage storage = new FlutterSecureStorage();
    String? token = await storage.read(key: 'token_key');
    if (token is String) {
      DateTime? expiry = Jwt.getExpiryDate(token);
      if (expiry is DateTime && expiry.isAfter(DateTime.now())) {
        return true;
      }
    }
  } catch (e) {
    print(e);
  }
  return false;
}

void main() {
  // String initialRoute = NavigationRouteName.auth;
  String initialRoute = '/splash';
  initializeDateFormatting('ko_KR', null).then((_) {
    runApp(
      GetMaterialApp(
        title: '스마트 부대 차량 관제 시스템',
        initialRoute: initialRoute,
        getPages: [
          GetPage(
            name: '/splash',
            page: () => Splash(),
          ),
          GetPage(
              name: NavigationRouteName.auth,
              page: () {
                Get.put(AppController());
                return LoginPage();
              }),
          GetPage(
              name: NavigationRouteName.home,
              page: () {
                Get.put(AppController());
                return Home();
              }),
          GetPage(
              name: NavigationRouteName.drive,
              page: () {
                Get.put(AppController());
                return DrivingPage();
              }),
        ],
        theme: ThemeData(
          primaryColor: Colors.white,
          backgroundColor: Colors.white,
          canvasColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
        ),
      ),
    );
  });
}

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    checkToken().then((value) {
      if (value) {
        Get.offAllNamed(NavigationRouteName.home);
      } else {
        Get.offAllNamed(NavigationRouteName.auth);
      }
    });
    return Container();
  }
}
