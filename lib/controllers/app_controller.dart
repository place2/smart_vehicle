import 'dart:async';
import 'dart:convert';
import 'package:get/get.dart';
import 'package:geolocator/geolocator.dart';
import 'package:smart_vehicles/data/driving_status_model.dart';
import 'package:smart_vehicles/data/operation_model.dart';
import 'package:smart_vehicles/controllers/api.dart';

class AppController extends GetxService {
  final API _apiService = Get.put(API());

  static AppController get shared => Get.find();

  static API get api {
    return shared._apiService;
  }

  Future signup(
    String drivingLevel,
    String id,
    String militaryRank,
    String password,
    String phoneNo,
    String unitName,
    String userFullName,
  ) async {
    return _apiService.postSignup(drivingLevel, id, militaryRank, password,
        phoneNo, unitName, userFullName);
  }

  Future login(
    String id,
    String password,
  ) async {
    return _apiService.postLogin(id, password);
  }

  Future refreshPosition() async {
    bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      print('GPS Service Unavailable.');
    }

    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        print('Location permissions are denied');
      }
    }

    currentPosition = await Geolocator.getCurrentPosition();
  }

  Position? currentPosition;

  Future driveStart(OperationModel plan) async {
    await refreshPosition();

    var response = await _apiService.postDrivingStart(plan.id);
    if (response != null && response.statusCode == 200) {
      currentPlan = plan;
      return true;
    }

    return false;
  }

  Future driveFinish() async {
    await refreshPosition();

    var response = await _apiService.postDrivingFinish(
      currentPlan!.id,
      currentPosition!.latitude,
      currentPosition!.longitude,
    );
    if (response != null && response.statusCode == 200) {
      currentPlan = null;
      return true;
    }

    return false;
  }

  OperationModel? currentPlan;
}
