import 'dart:typed_data';
import 'dart:convert';
import 'package:get/get.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decode/jwt_decode.dart';

const kBaseUrl = 'http://web.southpaw.kr:6001/';
const kTokenKey = 'token_key';

class API extends GetConnect {
  String? _token;
  String? _userFullName;

  dynamic convertResponse(var data) {
    if (data is String) {
      return utf8.decode(data.codeUnits);
    } else if (data is Map) {
      for (var key in data.keys) {
        data[key] = convertResponse(data[key]);
      }
      return data;
    } else if (data is List) {
      for (int i = 0; i < data.length; i++) {
        data[i] = convertResponse(data[i]);
      }
      return data;
    } else {
      return data;
    }
  }

  Future checkToken() async {
    var token = await FlutterSecureStorage().read(key: kTokenKey);
    if (token is String) {
      _token = token;
      Map<String, dynamic> decoded = Jwt.parseJwt(token);
      _userFullName = decoded['sub'];
    }
  }

  @override
  void onInit() {
    httpClient.addResponseModifier((request, response) {
      if (response.body != null && response.body is Map) {
        var body = response.body as Map;
        if (body['data'] != null) {
          var data = body['data'];
          data = convertResponse(data);
          body['data'] = data;
        }
        return body;
      }
      return null;
    });
    checkToken();
    super.onInit();
  }

  Future postSignup(
    String drivingLevel,
    String id,
    String militaryRank,
    String password,
    String phoneNo,
    String unitName,
    String userFullName,
  ) async {
    final url = kBaseUrl + 'api/app/user/add';

    final body = {
      'drivingLevel': drivingLevel,
      'id': id,
      'millitaryRank': militaryRank,
      'password': password,
      'phoneNumber': phoneNo,
      'unitName': unitName,
      'userFullName': userFullName,
    };

    final response = await post(
      url,
      {},
      query: body,
    );
    print('API signup response :\n\t${response.body}');
    return response;
  }

  Future postLogin(String id, String password) async {
    final url = kBaseUrl + 'api/app/login';

    final body = {
      'id': id,
      'password': password,
    };

    final response = await post(url, {}, query: body);

    print('API Login response :\n\t${response.body}');
    if (response.statusCode == 200 && response.body['data'] != null) {
      Map<String, dynamic> data = response.body['data'];
      _token = data['token'];
      _userFullName = data['userFullName'];

      FlutterSecureStorage().write(key: kTokenKey, value: _token);
    }

    return response;
  }

  Future getDashboardList() async {
    if (_token == null || _token!.isEmpty) {
      await checkToken();
    }
    final url = kBaseUrl + 'api/app/dashboard/list';

    final body = {
      'userFullName': _userFullName!,
    };

    final response = await get(url,
        query: body, headers: {'Authorization': 'Bearer ${_token!}'});

    print('API Dashboard List response :\n\t${response.body}');

    return response;
  }

  Future getDashboardHistory({int page = 0}) async {
    if (_token == null || _token!.isEmpty) {
      await checkToken();
    }
    final url = kBaseUrl + 'api/app/dashboard/history';

    final body = {
      'userFullName': _userFullName!,
      'size': '5',
    };
    if (page != 0) {
      body['currentPage'] = '$page';
    }

    final response = await get(url,
        query: body, headers: {'Authorization': 'Bearer ${_token!}'});

    print('API Dashboard History : query $body');
    print('API Dashboard History response :\n\t${response.body}');

    return response;
  }

  Future getDashboardDetail(int operationId) async {
    final url = kBaseUrl + 'api/app/dashboard/detail';

    final body = {
      'carsInOperationID': '$operationId',
    };

    final response = await get(url,
        query: body, headers: {'Authorization': 'Bearer ${_token!}'});

    print('API Dashboard Detail response :\n\t${response.body}');
    return response;
  }

  Future postDrivingStart(int operationId) async {
    final url = kBaseUrl + 'api/app/driving/start';

    final body = {
      'carsInOperationID': '$operationId',
    };

    final response = await post(url, {},
        query: body, headers: {'Authorization': 'Bearer ${_token!}'});
    print('API Driving Start response :\n\t${response.body}');

    return response;
  }

  Future postDrivingFinish(int operationId, double lat, double lng) async {
    final url = kBaseUrl + 'api/app/driving/finish';

    final body = {
      'carsInOperationID': '$operationId',
      'latitude': '$lat',
      'longitude': '$lng',
    };

    final response = await post(url, {},
        query: body, headers: {'Authorization': 'Bearer ${_token!}'});
    print('API Driving Finish response :\n\t${response.body}');

    return response;
  }

  Future getMapDetail(int operationId, double lat, double lng) async {
    final url = kBaseUrl + 'api/app/map/detail';

    final body = {
      'carsInOperationID': '$operationId',
      'latitude': '$lat',
      'longitude': '$lng',
    };

    final response = await get(url,
        query: body, headers: {'Authorization': 'Bearer ${_token!}'});
    print('API Map Detail response :\n\t${response.body}');

    return response;
  }
}
