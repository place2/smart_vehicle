import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_vehicles/controllers/app_controller.dart';
import 'package:smart_vehicles/data/operation_model.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';

typedef void intCallback(int index);

class SafetyConfirmController extends GetxController {
  static const int checkItemCount = 5;
  List<RxBool> checkedList = [
    false.obs,
    false.obs,
    false.obs,
    false.obs,
    false.obs,
  ];

  bool isChecked(int index) {
    return checkedList[index].value;
  }

  void check(int index, bool checked) {
    checkedList[index].value = checked;
  }

  bool get isCapable {
    for (var item in checkedList) {
      if (!item.value) {
        return false;
      }
    }
    return true;
  }

  RxBool _isWaiting = false.obs;
  bool get isWaiting => _isWaiting.value;

  void beginDrive(BuildContext context, OperationModel plan) async {
    dynamic ret = await begin(plan);
    if (ret != null) {
      if (ret is bool && ret) {
        Get.offAllNamed(NavigationRouteName.drive);
      }
      if (ret is String) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(ret),
          backgroundColor: Colors.red.withOpacity(0.8),
          duration: Duration(seconds: 1),
        ));
      }
    }
  }

  Future begin(OperationModel plan) async {
    if (isCapable) {
      _isWaiting.value = true;

      var result = await AppController.shared.driveStart(plan);
      _isWaiting.value = false;
      if (result is bool) {
        return result;
      }
    }
    return null;
  }

  Widget safetyItem(int index, String safetyCaption) {
    return Container(
      height: Numbers.safetyCheckIconSize,
      child: Row(
        children: [
          InkWell(
            onTap: () => check(
              index,
              !isChecked(index),
            ),
            child: Obx(
              () => Icon(
                Icons.check_circle,
                color: isChecked(index)
                    ? AppColors.mainBlue
                    : AppColors.iconDisabledGray,
                size: Numbers.safetyCheckIconSize,
              ),
            ),
          ),
          SizedBox(width: 14),
          Text(
            safetyCaption,
            style: TextStyle(
              color: AppColors.textBlack,
              fontSize: Numbers.defaultButtonCaptionFontSize,
              fontWeight: FontWeight.normal,
              height: 1.2,
            ),
          ),
        ],
      ),
    );
  }
}
