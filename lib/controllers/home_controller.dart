import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_vehicles/data/model.dart';
import 'package:smart_vehicles/data/operation_model.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';
import 'package:smart_vehicles/details.dart';
import 'package:smart_vehicles/tools/extensions.dart';
import 'package:smart_vehicles/safety_confirm.dart';

class HomeController extends GetxController {
  RxList _plans = <OperationModel>[].obs;
  RxList get plans => _plans;

  RxList _history = <OperationModel>[].obs;
  RxList get history => _history;

  @override
  void onInit() {
    super.onInit();
    refreshPlans();
    refreshHistory();
  }

  bool get hasTodayPlan {
    DateTime today = DateTime.now();
    for (var plan in _plans) {
      if (plan.startTime.sameDay(today)) {
        return true;
      }
    }
    return false;
  }

  void refreshPlans() async {
    _plans.clear();
    var response = await Model().operations();
    if (response is List<OperationModel>) {
      for (int i = 0; i < response.length; i++) {
        var plan = response[i];
        if (i == 0 || !plan.startTime.sameDay(response[i - 1].startTime)) {
          _plans.add(OperationPlanDate(plan.startTime));
        }
        _plans.add(plan);
      }
      update();
    }
  }

  RxBool loadFinished = false.obs;

  void refreshHistory() async {
    _history.clear();
    var response = await Model().history();
    if (response is List<OperationModel>) {
      _history.addAll(response);
      loadFinished.value = Model().historyPage == Model().maxPage;
    } else {
      loadFinished.value = true;
    }
    update();
  }

  void moreHistory() async {
    _isWaitingMore.value = true;
    var response = await Model().moreHistory();
    if (response is List<OperationModel>) {
      _history.addAll(response);
      loadFinished.value = Model().historyPage == Model().maxPage;
    } else {
      loadFinished.value = true;
    }
    _isWaitingMore.value = false;
    update();
  }

  RxBool _isWaitingMore = false.obs;
  bool get isWaitingMore => _isWaitingMore.value;
  set isWaitingMore(value) => _isWaitingMore.value = value;

  Widget operationList(BuildContext context) {
    return plans.isEmpty
        ? SliverToBoxAdapter(
            child: Container(
              height: 176,
              color: AppColors.backgroundGray,
              child: Center(
                child: Text(
                  Strings.planEmptyDescription,
                  style: TextStyle(
                    color: AppColors.textBlack,
                    fontSize: 15,
                    fontWeight: FontWeight.normal,
                    height: 1.2,
                  ),
                ),
              ),
            ),
          )
        : SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return cellForOperation(context, index);
              },
              semanticIndexCallback: (Widget widget, int localIndex) {
                if (localIndex.isEven) {
                  return localIndex ~/ 2;
                }
                return null;
              },
              childCount: plans.length * 2 + 1,
            ),
          );
  }

  Widget cellForOperation(BuildContext context, int index) {
    final int itemIndex = index ~/ 2;
    var prevItem = itemIndex == 0 ? null : plans[itemIndex - 1];
    var item = itemIndex >= plans.length ? null : plans[itemIndex];

    if (index.isOdd) {
      if (item is OperationPlanDate) {
        // Date Caption
        return Container(
          padding: EdgeInsets.symmetric(
            horizontal: Numbers.basicGap,
          ),
          height: Numbers.plansDateTimeHeight,
          width: double.infinity,
          color: AppColors.backgroundGray,
          child: Text(
            item.startTime.isToday()
                ? Strings.today
                : item.startTime.dateString(),
            style: TextStyle(
              fontSize: Numbers.plansDateTimeFontSize,
              color: item.startTime.isToday()
                  ? AppColors.textBlack
                  : AppColors.textGray,
              fontWeight: FontWeight.bold,
              height: 1.135,
            ),
            textAlign: TextAlign.left,
          ),
        );
      } else {
        // Opeartion Card
        return Container(
          padding: EdgeInsets.symmetric(horizontal: Numbers.basicGap),
          width: double.infinity,
          height: Numbers.plansItemHeight,
          color: AppColors.backgroundGray,
          child: cardForPlan(
            item,
            (plan) {
              Get.to(() => DetailPage(plan));
            },
            (plan) {
              Get.to(SafetyConfirm(plan));
            },
          ),
        );
      }
    } else {
      // Gaps
      if (item == null) {
        return Container(
          width: double.infinity,
          height: Numbers.plansBottomPadding,
          color: AppColors.backgroundGray,
        );
      } else if (item is OperationPlanDate) {
        return Container(
          width: double.infinity,
          height: Numbers.plansDateTopPadding,
          color: AppColors.backgroundGray,
        );
      } else {
        if (prevItem is OperationPlanDate) {
          return Container(
            width: double.infinity,
            height: Numbers.plansDateBottomPadding,
            color: AppColors.backgroundGray,
          );
        } else {
          return Container(
            width: double.infinity,
            height: Numbers.plansItemsGap,
            color: AppColors.backgroundGray,
          );
        }
      }
    }
  }

  Widget cardForPlan(
    OperationModel plan,
    Function(OperationModel plan) callbackDetail,
    Function(OperationModel plan) callbackBegin,
  ) {
    DateTime now = DateTime.now();
    print('now : $now');
    print('plan.time : ${plan.startTime}');
    print('isAfter: ${plan.startTime.isAfter(now)}');
    return Container(
      height: Numbers.plansItemHeight,
      padding: EdgeInsets.fromLTRB(20, 28, 20, 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(Numbers.defaultRadius),
      ),
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 18,
            child: Row(
              children: [
                SizedBox(
                  width: 60,
                  child: Text(
                    Strings.planCaptionTime,
                    style: TextStyle(
                      fontSize: Numbers.planCaptionFontSize,
                      color: AppColors.textGray,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
                Text(
                  plan.startTime.timeString(),
                  style: TextStyle(
                    fontSize: Numbers.planButtonTextFontSize,
                    color: AppColors.textBlack,
                    fontWeight: FontWeight.normal,
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 12),
          Container(
            width: double.infinity,
            height: 18,
            child: Row(
              children: [
                SizedBox(
                  width: 60,
                  child: Text(
                    Strings.planCaptionStart,
                    style: TextStyle(
                      fontSize: Numbers.planCaptionFontSize,
                      color: AppColors.textGray,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
                Text(
                  plan.startPointName,
                  style: TextStyle(
                    fontSize: Numbers.planButtonTextFontSize,
                    color: AppColors.textBlack,
                    fontWeight: FontWeight.normal,
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 12),
          Container(
            width: double.infinity,
            height: 18,
            child: Row(
              children: [
                SizedBox(
                  width: 60,
                  child: Text(
                    Strings.planCaptionDestination,
                    style: TextStyle(
                      fontSize: Numbers.planCaptionFontSize,
                      color: AppColors.textGray,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
                Text(
                  plan.destinationName,
                  style: TextStyle(
                    fontSize: Numbers.planButtonTextFontSize,
                    color: AppColors.textBlack,
                    fontWeight: FontWeight.normal,
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 22),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: SizedBox(
                    // height: Numbers.planButtonHeight,
                    height: double.infinity,
                    child: TextButton(
                      onPressed: () => callbackDetail(plan),
                      child: Text(
                        Strings.planCaptionDetail,
                        style: TextStyle(
                          color: AppColors.textBlack,
                          fontSize: Numbers.planButtonTextFontSize,
                        ),
                      ),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            AppColors.backgroundGray),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(Numbers.defaultRadius),
                            side: BorderSide(color: AppColors.backgroundGray),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: SizedBox(
                    height: Numbers.planButtonHeight,
                    child: TextButton(
                      onPressed: () => callbackBegin(plan),
                      child: Text(
                        Strings.planCaptionBegin,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: Numbers.planButtonTextFontSize,
                        ),
                      ),
                      style: ButtonStyle(
                        backgroundColor: !plan.confirmStatus
                            ? MaterialStateProperty.all<Color>(
                                AppColors.mainBlue)
                            : MaterialStateProperty.all<Color>(
                                AppColors.translucentMainBlue),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(Numbers.defaultRadius),
                            side: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget historyList(BuildContext context) {
    return history.isEmpty
        ? SliverToBoxAdapter(
            child: Container(
              height: 170,
              child: Center(
                child: Text(
                  Strings.historyEmptyDescription,
                  style: TextStyle(
                    color: AppColors.textBlack,
                    fontSize: 15,
                    fontWeight: FontWeight.normal,
                    height: 1.2,
                  ),
                ),
              ),
            ),
          )
        : SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                final int itemIndex = index ~/ 2;
                var plan = history[itemIndex];
                if (index.isEven) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: Numbers.basicGap),
                    height: Numbers.historyCellHeight,
                    child: cellForHistory(plan),
                  );
                } else {
                  return SizedBox(height: Numbers.historyCellGap);
                }
              },
              semanticIndexCallback: (Widget widget, int localIndex) {
                if (localIndex.isEven) {
                  return localIndex ~/ 2;
                }
                return null;
              },
              childCount: history.length * 2 - 1,
            ),
          );
  }

  Widget cellForHistory(OperationModel plan) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: Numbers.basicGap),
      height: Numbers.historyCellHeight,
      decoration: BoxDecoration(
        color: AppColors.backgroundGray,
        borderRadius: BorderRadius.circular(Numbers.defaultRadius),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            plan.startTime.dateString(containsYear: true),
            style: TextStyle(
              color: AppColors.textGray,
              fontSize: Numbers.planButtonTextFontSize,
              fontWeight: FontWeight.normal,
            ),
          ),
          SizedBox(
            height: Numbers.historyCellTextGap,
          ),
          Row(
            children: [
              Text(
                plan.startPointName,
                style: TextStyle(
                  color: AppColors.textBlack,
                  fontSize: Numbers.planButtonTextFontSize,
                  fontWeight: FontWeight.normal,
                ),
              ),
              SizedBox(width: 10),
              Icon(
                Icons.arrow_forward,
                size: 18,
                color: AppColors.iconBlack,
              ),
              SizedBox(width: 8),
              Text(
                plan.destinationName,
                style: TextStyle(
                  color: AppColors.textBlack,
                  fontSize: Numbers.planButtonTextFontSize,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget moreButton() {
    return history.isEmpty || loadFinished.value
        ? Container()
        : InkWell(
            onTap: () {
              moreHistory();
            },
            child: Obx(
              () => Container(
                height: 50,
                child: Center(
                  child: isWaitingMore
                      ? SizedBox(
                          width: Numbers.defaultIndicatorSize,
                          height: Numbers.defaultIndicatorSize,
                          child: CircularProgressIndicator(
                            color: AppColors.mainBlue,
                          ),
                        )
                      : Text(
                          Strings.historyCaptionMore,
                          style: TextStyle(
                            fontSize: Numbers.plansDateTimeFontSize,
                            color: AppColors.textGray,
                            fontWeight: FontWeight.normal,
                          ),
                          textAlign: TextAlign.center,
                        ),
                ),
              ),
            ),
          );
  }
}

class OperationPlanDate extends OperationModel {
  OperationPlanDate(DateTime date)
      : super(
          0,
          date,
          '',
          '',
          '',
          '',
          date,
          date,
          0,
          0,
          [],
          0,
          '',
          '',
          '',
          '',
          '',
        );
}
