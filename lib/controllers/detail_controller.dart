import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:smart_vehicles/data/model.dart';
import 'package:smart_vehicles/data/operation_model.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';
import 'package:smart_vehicles/safety_confirm.dart';

class DetailPageController extends GetxController {
  static const double kCaptionFontSize = 15.0;

  Rx<OperationModel>? plan;
  DetailPageController();

  ScrollController scrollController = ScrollController();

  OverlayEntry? overlayEntryForPassengers;

  RxDouble _passengersInfoPosition = 0.0.obs;
  double get passengersInfoPosition => _passengersInfoPosition.value;
  set passengersInfoPosition(double value) =>
      _passengersInfoPosition.value = value;

  RxBool _isShowPassengers = false.obs;
  bool get isShowPassengers => _isShowPassengers.value;
  set isShowPassengers(bool value) => _isShowPassengers.value = value;

  void updateDetails() async {
    var response = await Model().details(plan!.value.id);
    if (response != null) {
      if (plan == null) {
        plan = (response as OperationModel).obs;
      } else {
        plan!.value = response as OperationModel;
      }
    }
  }

  void backPressed() {
    Get.back();
  }

  void beginPressed() {
    Get.to(SafetyConfirm(plan!.value));
  }

  Widget cell(String caption, String value) {
    return Row(
      children: [
        Text(
          caption,
          style: TextStyle(
            color: AppColors.textGray,
            fontSize: kCaptionFontSize,
            fontWeight: FontWeight.normal,
          ),
        ),
        Spacer(),
        Text(
          value,
          style: TextStyle(
            color: AppColors.textBlack,
            fontSize: kCaptionFontSize,
            fontWeight: FontWeight.normal,
          ),
        ),
      ],
    );
  }

  GlobalKey keyForPassengers = GlobalKey();
  Widget cellForPassengers(BuildContext context, String caption, String value) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (isShowPassengers) {
          closePassengers();
        } else {
          showPassengers(context);
        }
      },
      child: Row(
        key: keyForPassengers,
        children: [
          Text(
            caption,
            style: TextStyle(
              color: AppColors.textGray,
              fontSize: kCaptionFontSize,
              fontWeight: FontWeight.normal,
            ),
          ),
          Spacer(),
          Text(
            value,
            style: TextStyle(
              color: AppColors.textBlack,
              fontSize: kCaptionFontSize,
              fontWeight: FontWeight.normal,
            ),
          ),
          SizedBox(width: 4.0),
          Obx(
            () => RotatedBox(
              quarterTurns: isShowPassengers ? 2 : 0,
              child: Icon(
                Icons.arrow_drop_down_circle_outlined,
                color: Color(0xFF8F9CAA),
                size: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

  OverlayEntry _createOverlayForPassengers(List<String> passengers) {
    String passengersString = passengers.join(', ');
    return OverlayEntry(
      builder: (context) {
        double screenWidth = MediaQuery.of(context).size.width;
        return Obx(
          () => Positioned(
            left: 0,
            top: passengersInfoPosition,
            width: screenWidth,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () {
                    closePassengers();
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 20, top: 4),
                    constraints: BoxConstraints(
                        minWidth: 120, maxWidth: screenWidth - 115),
                    padding: EdgeInsets.fromLTRB(16, 12, 8, 12),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: Colors.black.withOpacity(0.06))
                      ],
                      borderRadius:
                          BorderRadius.circular(Numbers.defaultRadius),
                      border: Border.all(color: Color(0xFFD3DAE2)),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          constraints:
                              BoxConstraints(maxWidth: screenWidth - 170),
                          child: Text(
                            passengersString,
                            style: TextStyle(
                              color: Color(0xFF3F434B),
                              fontSize: 13.0,
                              fontWeight: FontWeight.normal,
                              decoration: TextDecoration.none,
                              height: 1.5,
                            ),
                            textAlign: TextAlign.left,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                          ),
                        ),
                        SizedBox(width: 12.0),
                        GestureDetector(
                          onTap: () {
                            closePassengers();
                          },
                          child: Icon(
                            Icons.close,
                            size: 16,
                            color: Color(0xFF66738F),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void showPassengers(BuildContext context) {
    if (overlayEntryForPassengers == null) {
      overlayEntryForPassengers =
          _createOverlayForPassengers(plan!.value.passengers);
    }

    RenderBox renderBox =
        keyForPassengers.currentContext!.findRenderObject()! as RenderBox;
    Offset offset = renderBox.localToGlobal(Offset.zero);
    double position = renderBox.size.height + offset.dy;
    passengersInfoPosition = position;

    Overlay.of(context)?.insert(overlayEntryForPassengers!);
    isShowPassengers = true;

    scrollController.addListener(checkPassengerPosition);
  }

  void closePassengers() {
    overlayEntryForPassengers!.remove();
    overlayEntryForPassengers = null;
    isShowPassengers = false;
    scrollController.removeListener(checkPassengerPosition);
  }

  void checkPassengerPosition() {
    RenderBox renderBox =
        keyForPassengers.currentContext!.findRenderObject()! as RenderBox;
    Offset offset = renderBox.localToGlobal(Offset.zero);
    double position = renderBox.size.height + offset.dy;
    passengersInfoPosition = position;
  }
}
