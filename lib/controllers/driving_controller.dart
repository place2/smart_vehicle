import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_vehicles/controllers/app_controller.dart';
import 'package:smart_vehicles/data/model.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';
import 'package:smart_vehicles/data/driving_status_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:smart_vehicles/dialogs/destination_dialog.dart';

class DrivingPageController extends GetxController {
  static const Duration kTimeDuration = Duration(seconds: 1);
  static const int kTickCheck = 10;

  AppController _appController = Get.find();

  Rx<Duration> elapsed = Duration.zero.obs;
  late DateTime startTime;
  Rx<LatLng> current = LatLng(AppController.shared.currentPosition!.latitude,
          AppController.shared.currentPosition!.longitude)
      .obs;

  Timer? _timer;
  int _tick = 0;
  Rx<DrivingNotificationModel>? notification;

  void closeNotification() {
    notification = null;
    update();
  }

  void cancelTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    }
  }

  void startStepTimer() {
    startTime = DateTime.now();

    _timer = Timer.periodic(kTimeDuration, (timer) {
      if (_timer == null) {
        return;
      }
      tickHandler(timer);

      elapsed.value = DateTime.now().difference(startTime);
      update();
    });
  }

  void tickHandler(Timer timer) async {
    _tick++;
    if (_tick >= 10) {
      var response = await Model().drivingStatus();
      if (response is DrivingStatusModel) {
        current.value = LatLng(
          _appController.currentPosition!.latitude,
          _appController.currentPosition!.longitude,
        );
        DrivingStatusModel status = response;
        nextDestination.value = status.nextTpName;
        nextDistance.value = status.nextTpDistance;
        timeSecond.value = status.timeSecond;
        if (status.notification != null) {
          if (notification == null) {
            notification = status.notification!.obs;
          } else {
            notification!.value = status.notification!;
          }
        } else {
          // notification == null;
        }
      }
      double zoom = await _mapController!.getZoomLevel();
      CameraPosition camera = CameraPosition(target: current.value, zoom: zoom);
      _mapController?.animateCamera(CameraUpdate.newCameraPosition(camera));
      _tick = 0;
    }
    update();
  }

  LatLng? startPosition;
  LatLng? destinationPosition;
  List<TP> tpList = <TP>[].obs;

  RxString nextDestination = ''.obs;
  RxDouble nextDistance = 0.0.obs;
  RxInt timeSecond = 0.obs;

  void initialize() async {
    var response = await Model().drivingStatus();
    if (response is DrivingStatusModel) {
      current.value = LatLng(
        _appController.currentPosition!.latitude,
        _appController.currentPosition!.longitude,
      );
      traceList.add(current.value);
      DrivingStatusModel status = response;
      startPosition = LatLng(status.startLat, status.startLng);
      destinationPosition = LatLng(status.destLat, status.destLng);
      if (status.tpList != null && status.tpList!.isNotEmpty) {
        tpList.addAll(status.tpList!);
      }

      nextDestination.value = status.nextTpName;
      nextDistance.value = status.nextTpDistance;
      timeSecond.value = status.timeSecond;

      await loadIcons();
      setMarkersAndLines();
      startStepTimer();
    }
  }

  @override
  void onInit() {
    super.onInit();
    initialize();
  }

  @override
  void onClose() {
    cancelTimer();
    super.onClose();
  }

  Future finishDrive() async {
    await _appController.driveFinish();
    return true;
  }

  GoogleMapController? _mapController;
  set mapController(value) {
    _mapController = value;
  }

  RxBool loadedIcon = false.obs;
  BitmapDescriptor? startIcon;
  BitmapDescriptor? endIcon;
  BitmapDescriptor? carIcon;
  List<BitmapDescriptor> tpIcons = [];

  List<LatLng> traceList = [];
  Set<Marker> _markers = {};
  Set<Polyline> _polylines = {};

  Set<Marker> get markers {
    Set<Marker> ret = <Marker>{};
    ret.addAll(_markers);
    ret.add(
      Marker(
        markerId: MarkerId('car'),
        position: current.value,
        icon: carIcon!,
        anchor: Offset(0.5, 0.5),
        zIndex: 1,
      ),
    );
    return ret;
  }

  Set<Polyline> get polyLines {
    Set<Polyline> ret = <Polyline>{};
    ret.addAll(_polylines);
    List<LatLng> points = [];
    points.addAll(traceList);
    points.add(current.value);
    ret.add(
      Polyline(
          polylineId: PolylineId('car_path'),
          color: AppColors.routePathGreen,
          points: points,
          startCap: Cap.roundCap,
          endCap: Cap.roundCap,
          width: 3),
    );

    points.clear();
    points.add(current.value);
    for (var tp in tpList) {
      if (tp.name == nextDestination.value) {
        points.add(LatLng(tp.lat, tp.lng));
      }
    }
    ret.add(
      Polyline(
          polylineId: PolylineId('car_path'),
          patterns: [PatternItem.dash(7), PatternItem.gap(7)],
          color: AppColors.routePathGreen,
          points: points,
          startCap: Cap.roundCap,
          endCap: Cap.roundCap,
          width: 4),
    );

    return ret;
  }

  Future loadIcons() async {
    double pRatio = ui.window.devicePixelRatio;

    const double kIconSize = 60.0;
    carIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
            devicePixelRatio: pRatio, size: Size(kIconSize, kIconSize)),
        'assets/car.png');

    for (var item in tpList) {
      var tpImage = await generateTPImage('TP ${item.tpId}');
      tpIcons.add(tpImage);
    }
    startIcon = await generatePlaceImage('츌발');
    endIcon = await generatePlaceImage('도착');
    loadedIcon.value = true;
  }

  void setMarkersAndLines() {
    _markers.add(
      Marker(
        markerId: MarkerId('start'),
        position: startPosition!,
        icon: startIcon!,
        anchor: Offset(0.5, 0.83),
      ),
    );
    _markers.add(
      Marker(
        markerId: MarkerId('destination'),
        position: destinationPosition!,
        icon: endIcon!,
        anchor: Offset(0.5, 0.83),
      ),
    );
    int index = 0;
    for (TP tp in tpList) {
      _markers.add(
        Marker(
          markerId: MarkerId('tp${tp.tpId}'),
          position: LatLng(tp.lat, tp.lng),
          icon: tpIcons[index],
          anchor: Offset(0.5, 0.55),
        ),
      );
      index++;
    }
    List<LatLng> points = [];
    points.add(startPosition!);
    tpList.forEach((element) {
      points.add(LatLng(element.lat, element.lng));
    });
    points.add(destinationPosition!);
    _polylines.add(
      Polyline(
          polylineId: PolylineId('route_path'),
          patterns: [PatternItem.dash(10)],
          color: AppColors.routePathGreen,
          points: points,
          startCap: Cap.roundCap,
          endCap: Cap.roundCap,
          width: 3),
    );
  }

  String printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }

  Future generateTPImage(String tp) async {
    double pRatio = ui.window.devicePixelRatio.floorToDouble();

    final ByteData data = await rootBundle.load('assets/place.png');
    final ui.Codec codec = await ui.instantiateImageCodec(
      Uint8List.view(data.buffer),
      targetWidth: (60 * pRatio).floor(),
      targetHeight: (60 * pRatio).floor(),
    );
    final ui.Image placeImage = (await codec.getNextFrame()).image;

    final recorder = ui.PictureRecorder();
    final canvas = Canvas(recorder);
    canvas.drawImage(placeImage, Offset(0.0, 9.0 * pRatio), Paint());

    Paint bluePaint = Paint()
      ..color = AppColors.mainBlue
      ..style = PaintingStyle.fill;
    canvas.drawRRect(
        RRect.fromLTRBR(
          7.0 * pRatio,
          0.0,
          52.0 * pRatio,
          25.0 * pRatio,
          Radius.circular(5.0 * pRatio),
        ),
        bluePaint);

    Path path = Path()
      ..moveTo(28 * pRatio, 25 * pRatio)
      ..lineTo(30 * pRatio, 29 * pRatio)
      ..lineTo(32 * pRatio, 25 * pRatio)
      ..close();
    canvas.drawPath(path, bluePaint);

    final textPainter = TextPainter(
      text: TextSpan(
        text: tp,
        style: TextStyle(
          color: Colors.white,
          fontSize: 13.0 * pRatio,
          fontWeight: FontWeight.w800,
        ),
      ),
      textDirection: ui.TextDirection.ltr,
    );
    textPainter.layout();
    textPainter.paint(canvas, Offset(16 * pRatio, 5 * pRatio));

    final ui.Picture picture = recorder.endRecording();
    final ui.Image image =
        await picture.toImage((60 * pRatio).floor(), (69 * pRatio).floor());
    final byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    final Uint8List rawBytes = Uint8List.view(byteData!.buffer);

    return BitmapDescriptor.fromBytes(rawBytes);
  }

  Future generatePlaceImage(String place) async {
    double pRatio = ui.window.devicePixelRatio.floorToDouble();

    final recorder = ui.PictureRecorder();
    final canvas = Canvas(recorder);

    Paint whitePaint = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill;
    Paint bluePaint = Paint()
      ..color = AppColors.mainBlue
      ..style = PaintingStyle.fill;
    canvas.drawRRect(
        RRect.fromLTRBR(
          0.0 * pRatio,
          0.0,
          49.0 * pRatio,
          25.0 * pRatio,
          Radius.circular(5.0 * pRatio),
        ),
        whitePaint);

    Path path = Path()
      ..moveTo(22.5 * pRatio, 25 * pRatio)
      ..lineTo(24.6 * pRatio, 29 * pRatio)
      ..lineTo(26.5 * pRatio, 25 * pRatio)
      ..close();
    canvas.drawPath(path, whitePaint);

    final textPainter = TextPainter(
      text: TextSpan(
        text: place,
        style: TextStyle(
          color: AppColors.mainBlue,
          fontSize: 13.0 * pRatio,
          fontWeight: FontWeight.w800,
        ),
      ),
      textDirection: ui.TextDirection.ltr,
    );
    textPainter.layout();
    textPainter.paint(canvas, Offset(11.5 * pRatio, 5 * pRatio));

    canvas.drawCircle(
        Offset(24.5 * pRatio, 39 * pRatio), 8 * pRatio, bluePaint);
    canvas.drawCircle(
        Offset(24.5 * pRatio, 39 * pRatio), 3 * pRatio, whitePaint);

    final ui.Picture picture = recorder.endRecording();
    final ui.Image image =
        await picture.toImage((49 * pRatio).floor(), (47 * pRatio).floor());
    final byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    final Uint8List rawBytes = Uint8List.view(byteData!.buffer);

    return BitmapDescriptor.fromBytes(rawBytes);
  }

  Widget widgetForNotification(BuildContext context) {
    if (notification != null) {
      if (notification!.value.type == 1 || notification!.value.type == 2) {
        return Container(
          padding: EdgeInsets.fromLTRB(23, 0, 23, 16),
          width: double.infinity,
          height: 90,
          color: Colors.transparent,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 16),
            width: double.infinity,
            height: 74,
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.8),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        kMapStatusNotiTitle[notification!.value.type - 1],
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          height: 1.2,
                        ),
                      ),
                      SizedBox(height: 6),
                      Text(
                        kMapStatusNotiMessage[notification!.value.type - 1],
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w300,
                          height: 1.2,
                        ),
                      ),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    notification = null;
                    update();
                  },
                  child: Icon(
                    Icons.close,
                    size: 20,
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
        );
      }
    }
    return Container();
  }
}
