import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_vehicles/controllers/app_controller.dart';
import 'package:smart_vehicles/signup.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';

class LoginPageController extends GetxController {
  final TextEditingController idController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  RxBool _isError = false.obs;
  bool get isError => _isError.value;
  set isError(value) => _isError.value = value;

  RxBool _isObscure = true.obs;
  bool get isObscurePassword => _isObscure.value;
  set isObscurePassword(value) => _isObscure.value = value;

  RxBool _isCapable = false.obs;
  bool get isCapable => _isCapable.value;

  RxBool _isWaiting = false.obs;
  bool get isWaiting => _isWaiting.value;

  void checkCapable() {
    _isCapable.value =
        idController.text.isNotEmpty && passwordController.text.isNotEmpty;
  }

  Future login(BuildContext context) async {
    dynamic ret = await submit(idController.text, passwordController.text);
    if (ret != null) {
      if (ret is bool && ret) {
        Get.offAllNamed('/home');
      }
      if (ret is String) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(ret),
          backgroundColor: Colors.red.withOpacity(0.8),
          duration: Duration(seconds: 1),
        ));
      }
    }
  }

  Future submit(String id, String password) async {
    if (_isCapable.value) {
      AppController appController = Get.find();
      _isWaiting.value = true;
      var response = await appController.login(id, password);
      if (response.statusCode == 200) {
        Map<String, dynamic> body = response.body;
        if (body['code'] != null && body['code'] == 200) {
          _isWaiting.value = false;
          return true;
        }
      }
    }
    _isWaiting.value = false;
    _isError.value = true;
    return null;
  }

  void apply() async {
    Get.to(ApplyPage());
  }

  Widget errorWidget() {
    return Obx(
      () {
        return isError
            ? Container(
                child: Row(
                  children: [
                    Icon(
                      Icons.error_outline,
                      size: 16,
                      color: AppColors.iconAlertRed,
                    ),
                    SizedBox(width: 4),
                    Text(
                      Strings.loginError,
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.normal,
                        color: AppColors.textAlertRed,
                      ),
                    ),
                  ],
                ),
              )
            : Container();
      },
    );
  }

  Widget buttonLabel() {
    const double defaultFontSize = 15.0;
    return Obx(
      () => Center(
        child: isWaiting
            ? SizedBox(
                width: Numbers.defaultIndicatorSize,
                height: Numbers.defaultIndicatorSize,
                child: CircularProgressIndicator(
                  color: Colors.white,
                ),
              )
            : Text(
                Strings.login,
                style: TextStyle(
                  fontSize: defaultFontSize,
                  color: Colors.white,
                  fontWeight: FontWeight.normal,
                ),
              ),
      ),
    );
  }
}
