import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_vehicles/controllers/home_controller.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final HomeController _controller = Get.put(HomeController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: RichText(
          text: TextSpan(
              style: TextStyle(
                fontSize: Numbers.defaultAppBarTitleSize,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
              children: [
                TextSpan(
                  text: Strings.titleSmart,
                  style: TextStyle(
                    color: AppColors.textGreen,
                  ),
                ),
                TextSpan(
                  text: ' ${Strings.titleRemains}',
                ),
              ]),
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
        shadowColor: Colors.transparent,
      ),
      body: SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
          child: Obx(
            () => CustomScrollView(
              slivers: [
                _controller.operationList(context),
                SliverToBoxAdapter(
                  child: Container(
                    height: 66,
                    padding: EdgeInsets.fromLTRB(
                      Numbers.basicGap,
                      28,
                      Numbers.basicGap,
                      16,
                    ),
                    child: Text(
                      Strings.historyCaption,
                      style: TextStyle(
                        fontSize: Numbers.plansDateTimeFontSize,
                        color: AppColors.textBlack,
                        fontWeight: FontWeight.normal,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
                _controller.historyList(context),
                SliverToBoxAdapter(
                  child: SizedBox(height: _controller.history.isEmpty ? 0 : 16),
                ),
                SliverToBoxAdapter(
                  child: _controller.moreButton(),
                ),
                SliverToBoxAdapter(
                  child: SizedBox(height: _controller.history.isEmpty ? 0 : 16),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
