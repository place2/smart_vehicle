import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';

Future<dynamic> showEndConfirmDialog(BuildContext context) async {
  var ret = showDialog(
    context: context,
    barrierDismissible: true,
    builder: (context) {
      double totalWidth = MediaQuery.of(context).size.width;
      return AlertDialog(
        insetPadding: EdgeInsets.symmetric(horizontal: 20),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
        content: Container(
          width: totalWidth,
          height: 228,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(
                Icons.beenhere,
                size: 36,
                color: AppColors.mainBlue,
              ),
              SizedBox(height: 12),
              Text(
                Strings.drivingButtonCaption,
                style: TextStyle(
                  color: AppColors.textBlack,
                  fontSize: Numbers.defaultAppBarTitleSize,
                  fontWeight: FontWeight.bold,
                  height: 1.2,
                ),
              ),
              SizedBox(height: 8),
              Text(
                '목적지까지 운행이 완료되었습니까?',
                style: TextStyle(
                  color: AppColors.textBlack,
                  fontSize: Numbers.defaultButtonCaptionFontSize,
                  fontWeight: FontWeight.normal,
                  height: 1.2,
                ),
              ),
              SizedBox(height: 8),
              Row(
                children: [
                  Icon(
                    Icons.error_outline,
                    color: Color(0xFFD96161),
                    size: 16,
                  ),
                  SizedBox(width: 4),
                  Text(
                    '목적지가 아닌 다른 곳에서 종료를 금지합니다.',
                    style: TextStyle(
                      color: Color(0xFFD96161),
                      fontSize: 13,
                      fontWeight: FontWeight.normal,
                      height: 1.2,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 44,
                        child: TextButton(
                          onPressed: () {
                            Get.back();
                          },
                          child: Text(
                            '취소',
                            style: TextStyle(
                              color: AppColors.textBlack,
                              fontSize: Numbers.planButtonTextFontSize,
                            ),
                          ),
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                AppColors.backgroundGray),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                    Numbers.defaultRadius),
                                side:
                                    BorderSide(color: AppColors.backgroundGray),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: SizedBox(
                        height: 44,
                        child: TextButton(
                          onPressed: () {
                            Get.back(result: true);
                          },
                          child: Text(
                            Strings.drivingButtonCaption,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: Numbers.planButtonTextFontSize,
                            ),
                          ),
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                AppColors.mainBlue),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                    Numbers.defaultRadius),
                                side: BorderSide.none,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
  return ret;
}
