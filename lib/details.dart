import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:smart_vehicles/data/operation_model.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';
import 'package:smart_vehicles/controllers/detail_controller.dart';

class DetailPage extends StatelessWidget {
  final OperationModel plan;
  DetailPage(this.plan);

  final DetailPageController _controller = Get.put(DetailPageController());

  @override
  Widget build(BuildContext context) {
    if (_controller.plan == null) {
      _controller.plan = plan.obs;
      _controller.updateDetails();
    }
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: AppColors.iconBlack,
            size: Numbers.appBarIconSize,
          ),
          onPressed: () {
            _controller.dispose();
            _controller.backPressed();
          },
        ),
        title: Text(
          Strings.detailTitle,
          style: TextStyle(
            color: AppColors.textBlack,
            fontSize: Numbers.defaultAppBarTitleSize,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        shadowColor: Colors.transparent,
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: Numbers.basicGap),
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
          child: Obx(
            () {
              OperationModel _plan =
                  _controller.plan == null ? plan : _controller.plan!.value;
              return ListView(
                controller: _controller.scrollController,
                children: [
                  SizedBox(
                    height: 21,
                  ),
                  _controller.cell(Strings.detailCaptionCarType, _plan.carType),
                  SizedBox(height: 30),
                  _controller.cell(Strings.detailCaptionCarNO, _plan.carType),
                  SizedBox(height: 30),
                  _controller.cell(Strings.detailCaptionDriver, _plan.driver),
                  SizedBox(height: 30),
                  _controller.cell(Strings.detailCaptionLeader, _plan.leader),
                  SizedBox(height: 30),
                  _controller.cell(
                    Strings.detailCaptionStartTime,
                    DateFormat('yyyy년 MM월 dd일').format(_plan.startTime),
                  ),
                  SizedBox(height: 30),
                  _controller.cell(
                    Strings.detailCaptionArrivalTime,
                    DateFormat('yyyy년 MM월 dd일').format(_plan.arrivalTime),
                  ),
                  SizedBox(height: 30),
                  _controller.cell(
                    Strings.detailCaptionReturnTime,
                    DateFormat('yyyy년 MM월 dd일').format(_plan.returnTime),
                  ),
                  SizedBox(height: 30),
                  _controller.cell(
                      Strings.detailCaptionDistance, '${_plan.distance}km'),
                  SizedBox(height: 30),
                  _controller.cell(Strings.detailCaptionCargoWeight,
                      '${_plan.cargoWeight}kg'),
                  SizedBox(height: 30),
                  _controller.cellForPassengers(
                      context,
                      Strings.detailCaptionPassengers,
                      '${_plan.leader}(외${_plan.passengers.length - 1})'),
                  SizedBox(height: 30),
                  _controller.cell(
                      Strings.detailCaptionControlNo, '${_plan.controlNo}'),
                  SizedBox(height: 30),
                  _controller.cell(
                      Strings.detailCaptionDistanceType, _plan.distanceType),
                  SizedBox(height: 30),
                  _controller.cell(
                      Strings.detailCaptionPeopleOrCargo, _plan.peopleOrCargo),
                  SizedBox(height: 30),
                  _controller.cell(
                      Strings.detailCaptionPlanUnit, _plan.planUnit),
                  SizedBox(height: 30),
                  _controller.cell(
                      Strings.detailCaptionDestination, _plan.destinationName),
                  SizedBox(height: 40),
                  TextButton(
                    onPressed: () {
                      _controller.beginPressed();
                    },
                    child: Text(
                      Strings.safetyButtonCaption,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: Numbers.defaultButtonCaptionFontSize,
                        fontWeight: FontWeight.normal,
                        height: 1.2,
                      ),
                    ),
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(
                        EdgeInsets.symmetric(vertical: 15),
                      ),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(AppColors.mainBlue),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(Numbers.defaultRadius),
                          side: BorderSide.none,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 40),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
