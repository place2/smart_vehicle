class Numbers {
  // view numbers
  static double get basicGap => 20.0;
  static double get defaultRadius => 12.0;
  static double get defaultControlHeight => 48.0;
  static double get defaultIndicatorSize => 30.0;
  static double get obscureIconSize => 20.0;

  static double get appBarIconSize => 24.0;
  static double get defaultAppBarTitleSize => 18.0;
  static double get defaultCaptionSize => 13.0;
  static double get defaultButtonCaptionFontSize => 15.0;

  static double get plansDateTopPadding => 20.0;
  static double get plansDateBottomPadding => 8.0;
  static double get plansItemsGap => 12.0;
  static double get plansBottomPadding => 28.0;
  static double get plansDateTimeHeight => 22.0;
  static double get plansDateTimeFontSize => 18.0;
  static double get plansItemHeight => 192.0;
  static double get planCaptionFontSize => 13.0;
  static double get planButtonTextFontSize => 15.0;
  static double get planButtonHeight => 44.0;
  static double get historyCellHeight => 90.0;
  static double get historyCellGap => 12.0;
  static double get historyCellTextGap => 14.0;

  static double get safetyCheckIconSize => 28.0;
  static double get safetyButtonHeight => 44.0;
  static double get safetyCaptionFontSize => 15.0;
}

enum DriverClass {
  A,
  B,
  C,
}
