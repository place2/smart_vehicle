class NavigationRouteName {
  static String get auth => '/auth';
  static String get home => '/home';
  static String get drive => '/drive';
}

class Strings {
  // Login Page
  static String get titleSmart => '스마트';
  static String get titleRemains => '부대차량 관제시스템';
  static String get hintId => '아이디';
  static String get hintPassword => '비밀번호';
  static String get login => '로그인';
  static String get noAccount => '계정이 없으신가요?';
  static String get pleaseSingup => ' 가입 승인을 요청하세요.';
  static String get loginError => '아이디 또는 비밀번호가 잘못되었습니다.';

  // Signup Page
  static String get signupTitle => '가입 승인 신청';
  static String get captionName => '이름';
  static String get captionId => '아이디';
  static String get captionPhone => '전화번호';
  static String get captionPass => '비밀번호';
  static String get captionUnit => '부대명';
  static String get captionUnitUser => '부대별 사용자';
  static String get captionRank => '계급';
  static String get captionDriverClass => '운전등급';
  static String get hintName => '이름 입력';
  static String get hintIdInput => '아이디 입력';
  static String get hintPhone => '전화번호 입력';
  static String get hintPass => '비밀번호 입력';
  static String get hintUnit => '부대명 입력';
  static String get hintUnitUser => '부대별 사용자 입력';
  static String get hintRank => '본인 계급 입력';
  static List<String> get driverClassValues => ['A급', 'B급', 'C급'];
  static String get signupApply => '승인 신청';
  static String get signupError => '모든 항목을 입력해주세요.';

  // Home
  static String get today => '오늘';
  static String get planCaptionTime => '출발시간';
  static String get planCaptionStart => '출발지';
  static String get planCaptionDestination => '도착지';
  static String get planCaptionDetail => '상세보기';
  static String get planCaptionBegin => '운행시작';
  static String get historyCaption => '운행내역';
  static String get historyCaptionMore => '더보기';
  static String get planEmptyDescription => '운행 요청이 없습니다.';
  static String get historyEmptyDescription => '운행 내역이 없습니다.';

  // safety confirm
  static String get safetyConfirmTitle => '안전 사항 확인';
  static String get safetyCaption1 => '1. 안전벨트를 확인해 주세요.';
  static String get safetyCaption2 => '2. 동승자를 확인해 주세요.';
  static String get safetyCaption3 => '3. 타이어 상태를 확인해 주세요.';
  static String get safetyCaption4 => '4. 차량유류정보를 확인해 주세요.';
  static String get safetyCaption5 => '5. 사이드/백미러를 확인해 주세요.';
  static String get safetyButtonCaption => '운행 시작';

  static String get drivingButtonCaption => '운행 종료';

  // 배차 상세 내용
  static String get detailTitle => '배차 상세내용';
  static String get detailCaptionCarType => '차종';
  static String get detailCaptionCarNO => '차량번호';
  static String get detailCaptionDriver => '운전자';
  static String get detailCaptionLeader => '승차 책임자';
  static String get detailCaptionStartTime => '출발 일시';
  static String get detailCaptionArrivalTime => '목적지 도착일시';
  static String get detailCaptionReturnTime => '복귀 일시';
  static String get detailCaptionDistance => '운랭 거리';
  static String get detailCaptionCargoWeight => '화물 중량';
  static String get detailCaptionPassengers => '탑승 인원';
  static String get detailCaptionControlNo => '통제 번호';
  static String get detailCaptionDistanceType => '거리 구분';
  static String get detailCaptionPeopleOrCargo => '병력/화물';
  static String get detailCaptionPlanUnit => '계획 부대';
  static String get detailCaptionDestination => '도착지';
}

List<String> kMapStatusNotiTitle = [
  'TP 스킵 경고',
  '경로를 이탈하였습니다.',
];

List<String> kMapStatusNotiMessage = [
  '모든 TP를 순서대로 진행하셔야 합니다.',
  '계획된 경로로 진행하세요.',
];

List<String> kMilitaryUnit = [
  '여단',
  '1대대',
  '2대대',
  '3대대',
];

List<String> kMilitaryRank = [
  '군무원',
  '이병',
  '일병',
  '상병',
  '병장',
  '하사',
  '중사',
  '상사',
  '원사',
  '준위',
  '소위',
  '중위',
  '대위',
  '소령',
  '중령',
  '대령',
];
