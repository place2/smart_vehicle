import 'package:flutter/material.dart';

class AppColors {
  static Color get mainBlue => Color(0xFF0976DA);
  static Color get translucentMainBlue => mainBlue.withOpacity(0.5);
  static Color get inputBorderColor => Colors.white.withOpacity(0.2);

  static Color get textBlack => Color(0xFF1F1F1F);
  static Color get textGray => Color(0xFF66738F);
  static Color get textGreen => Color(0xFF00C773);
  static Color get textAlertRed => Color(0xFFD96161);

  static Color get iconTranslucentWhite => Colors.white.withOpacity(0.5);
  static Color get iconBlack => Color(0xFF1F1F1F);
  static Color get iconGray => Color(0xFFB7C2CE);
  static Color get iconDarkGray => Color(0xFF66738F);
  static Color get iconDisabledGray => Color(0xFFD3DAE2);
  static Color get iconAlertRed => Color(0xFFD96161);

  static Color get backgroundGray => Color(0xFFF4F5F9);

  static Color get routePathDarkGreen => Color(0xFF66738F);
  static Color get routePathGreen => Color(0xFF00C75B);
}
