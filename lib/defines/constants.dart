import 'dart:core';

const List<String> temp_classes = [
  '계급_1',
  '계급_2',
  '계급_3',
  '계급_4',
  '계급_5',
  '계급_6',
  '계급_7',
  '계급_8',
  '계급_9',
  '계급_10',
];

const List<String> temp_units = [
  '부대 1',
  '부대 2',
  '부대 3',
  '부대 4',
  '부대 5',
  '부대 6',
  '부대 7',
  '부대 8',
  '부대 9',
];
