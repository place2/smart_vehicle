import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_vehicles/controllers/app_controller.dart';
import 'package:smart_vehicles/controllers/driving_controller.dart';
import 'package:smart_vehicles/data/model.dart';
import 'package:smart_vehicles/defines/app_colors.dart';
import 'package:smart_vehicles/defines/app_strings.dart';
import 'package:smart_vehicles/defines/app_numbers.dart';
import 'package:smart_vehicles/data/driving_status_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:smart_vehicles/dialogs/destination_dialog.dart';

class DrivingPage extends StatefulWidget {
  @override
  _DrivingPageState createState() => _DrivingPageState();
}

class _DrivingPageState extends State<DrivingPage> with WidgetsBindingObserver {
  final DrivingPageController _controller = Get.put(DrivingPageController());

  late CameraPosition initialCamera;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addObserver(this);
    initialCamera = CameraPosition(
      target: LatLng(AppController.shared.currentPosition!.latitude,
          AppController.shared.currentPosition!.longitude),
      zoom: 12.3,
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    _controller.cancelTimer();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    print(devicePixelRatio);
    return Scaffold(
      body: Stack(
        children: [
          Obx(
            () => GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: initialCamera,
              onMapCreated: (GoogleMapController controller) {
                _controller.mapController = controller;
              },
              markers: _controller.loadedIcon.value ? _controller.markers : {},
              polylines:
                  _controller.loadedIcon.value ? _controller.polyLines : {},
            ),
          ),
          Column(
            children: [
              Spacer(),
              _controller.widgetForNotification(context),
              Container(
                padding: EdgeInsets.fromLTRB(20, 32, 20, 37),
                height: 177,
                width: width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
                  color: Colors.white,
                ),
                child: Container(
                  height: 36,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            width: 36,
                            height: 36,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                color: Color(0xFFD3DAE2),
                              ),
                            ),
                            child: Center(
                              child: Icon(
                                Icons.place,
                                color: AppColors.mainBlue,
                                size: 18,
                              ),
                            ),
                          ),
                          SizedBox(width: 12),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Obx(
                                () => Text(
                                  _controller.nextDestination.value,
                                  style: TextStyle(
                                    color: AppColors.textBlack,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              Obx(
                                () => Text(
                                  '${(_controller.nextDistance).toStringAsFixed(2)}Km',
                                  style: TextStyle(
                                    color: AppColors.textGray,
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ],
                          ),
                          Spacer(),
                          Column(
                            children: [
                              SizedBox(height: 14),
                              Obx(
                                () => Text(
                                  _controller
                                      .printDuration(_controller.elapsed.value),
                                  style: TextStyle(
                                    color: AppColors.textBlack,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Spacer(),
                      SizedBox(
                        width: double.infinity,
                        child: TextButton(
                          onPressed: () {
                            showEndConfirmDialog(context).then((value) {
                              if (value is bool && value) {
                                _controller.cancelTimer();
                                _controller.finishDrive();
                                Get.offAllNamed(NavigationRouteName.home);
                              }
                            });
                          },
                          child: Text(
                            Strings.drivingButtonCaption,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: Numbers.defaultButtonCaptionFontSize,
                              fontWeight: FontWeight.normal,
                              height: 1.2,
                            ),
                          ),
                          style: ButtonStyle(
                            padding: MaterialStateProperty.all<EdgeInsets>(
                              EdgeInsets.symmetric(vertical: 15),
                            ),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                AppColors.mainBlue),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                    Numbers.defaultRadius),
                                side: BorderSide.none,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
